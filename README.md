![CircleCI](https://circleci.com/bb/sasys/os-details.svg?style=svg)

# os-details



Component hides operating specific details.

## Usage

### Local

```
  lein install

  clojure -A:test -m os.test-details
```

### Desktop circleci

     circleci local execute --job build
