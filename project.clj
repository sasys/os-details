(defproject com.sbnl.util.os-details "0.1.0-SNAPSHOT"
   :description "A low level project that provides deatils of the operating system"
   :url "http://example.com/FIXME"
   :dependencies [[org.clojure/clojure      "1.10.1"]
                  [lein-autoexpect          "1.9.0"]
                  [expectations             "2.2.0-beta1"]
                  [org.clojure/tools.logging "0.5.0"]
                  [pjstadig/humane-test-output "0.10.0"]]

   :plugins [[lein-modules "0.3.11"]
             [lein-ancient "0.6.15"]]

  ;:middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]

  ;:lein-tools-deps/config {:config-files [:project]}

  :source-paths ["src"]

  :test-paths ["test"])
