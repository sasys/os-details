(ns os.test-details
  (:require [os.details   :refer [os-name home-dir]]
            [clojure.test :refer [is deftest testing]]))

(deftest name
  (is (false? (nil? (os-name)))))

(deftest options
  (if (= "osx" (os-name))
    (deftest osx
      (is (= "/Users" (home-dir))))
    (deftest linux
      (is (= "/home" (home-dir))))))

(defn -main []
  (clojure.test/run-tests 'os.test-details))
