(ns os.details

  "
   Provides operating system details.
   For OS home locations see https://en.wikipedia.org/wiki/Home_directory
  ")

;maps literal os name to java os name
(def os-name-map
  {"mac os x"  "osx"
   "windows 7" "windows7"
   "linux"     "linux"})

;maps literal os name to home location
(def home-loc
  {"osx"      "/Users"
   "windows7" nil
   "linux"    "/home"})


(defn os-name
  "Finds the common name of the current operating system"
  []
  (get os-name-map (.toLowerCase (System/getProperty "os.name"))))

(defn user-name
  "Finds the user name so we can get the home directory"
  []
  (System/getProperty "user.name"))

(defn home-dir
  "Finds the home location for this os"
  []
  (home-loc (os-name)))

(defn user-dir
  "Finds the user home directory"
  []
  (System/getProperty "user.home"))
